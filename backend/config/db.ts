import Mongoose from "mongoose";
import dotenv from "dotenv";
import path from "path";

dotenv.config({ path: path.resolve(__dirname, "../.env") });

let database: Mongoose.Connection;

//** Connexion à la BDD
export const connect = async (): Promise<void> => {
	if (database) {
		return;
	}
	const db: string = process.env.MONGO_URI as string;

	try {
		await Mongoose.connect(db, {
			useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false,
			useUnifiedTopology: true,
		});

		console.log("MongoDB Connecté !");
	} catch (err) {
		console.error(err.message);
		// Exit process with failure
		process.exit(1);
	}
};
