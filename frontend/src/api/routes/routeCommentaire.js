import api from "../api.js";


const get = (hash) => {
	return api.get(`/commentaires/${hash}`);
};

const create = (data) => {
	return api.post("/commentaire", data);
};

const remove = (id) => {
	return api.delete(`/commentaire/${id}`);
};

export default {
	remove,
	get,
	create,
};
