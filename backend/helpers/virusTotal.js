const fs = require("fs");
const VirusTotalApi = require("virustotal-api");
const virusTotal = new VirusTotalApi(
	"1ace6388cc32c855c9b6bf9c34acdf36f85160d9a4d6b26648bcef7ce0acd59e"
);
const path = require("path");

function scanFichier(fichier) {
	let resultatDuScan = true;
	return new Promise(function (resolve, reject) {
		//Chemin d'accès à mon fichier
		let filePathForScanning = `${path.resolve("././")}\\${fichier.filePath}`;
		//Lecture du fichier
		fs.readFile(filePathForScanning, (err, data) => {
			if (err) {
				console.log(`Impossible de lire le fichier. ${err}`);
			} else {
				//Scan du fichier
				virusTotal
					// je slice pour garder que le nom du fichier
					.fileScan(data, fichier.filePath.slice(8, -1))
					.then((response) => {
						let resource = response.resource;
						// Après le scan envoi du rapport
						virusTotal.fileReport(resource).then((result) => {
							//Si un virus est détecté dans le rapport, un message est envoyé
							const resultat = result.scans;
							//parser les values detected = True ou false
							for (let k in resultat) {
								if (resultat[k] instanceof Object) {
									if (resultat[k]["detected"] === true) {
										console.log("Virus détecté");
										resultatDuScan = true;
										resolve(resultatDuScan);
									} else {
										resultatDuScan = false;
									}
								}
							}
							console.log("Fin du rapport");
							resolve(resultatDuScan);
						});
					})
					.catch((err) => console.log(`Le scan a échoué. ${err}`));
			}
		});
	});
}

export default scanFichier;
