import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useParams } from "react-router";
import routeApplication from "../api/routes/routeApplication";
import LoadingScreen from "./LoadingScreen";
import ListeCommentaires from "./commentaire/ListeCommentaires";

const useStyles = makeStyles({
	root: {
		maxWidth: 345 * 3,
	},
	media: {
		height: 140 * 3,
	},
});

export default function OneApplication(props) {
	const classes = useStyles();
	const { hash } = useParams();
	const [application, setApplication] = useState();
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		getApplicationByHash(hash);
		setTimeout(() => setLoading(false), 4000);
	}, [hash]);

	//Méthode de récupération de l'application par le hash
	const getApplicationByHash = (hash) => {
		routeApplication
			.get(hash)
			.then((response) => {
				setApplication(response.data.application);
			})
			.catch((e) => {
				console.log(e);
			});
	};

	//Téléchargement de l'application
	const downloadApk = (hash) => {
		routeApplication
			.downloadApp(hash)
			.then(({ data }) => {
				//création d'un lien fictif et d'une action
				const downloadUrl = window.URL.createObjectURL(new Blob([data]));
				const link = document.createElement("a");
				link.href = downloadUrl;
				link.setAttribute("download", `${application.nom}.apk`); //any other extension
				document.body.appendChild(link);
				link.click();
				link.remove(); //suppression du lien fictif
			})
			.catch((e) => {
				console.log(e);
			});
	};

	return (
		<>
			{loading === false ? (
				<>
					<Card className={classes.root}>
						<CardActionArea>
							<CardMedia
								className={classes.media}
								image="/images/default.jpg"
								title="Contemplative Reptile"
							/>
							<CardContent>
								<Typography gutterBottom variant="h5" component="h2">
									{application.nom}
								</Typography>
								<Typography variant="body2" color="textSecondary" component="p">
									Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo
									debitis tenetur placeat aliquid? Libero quas minus neque
									cumque inventore dolore similique harum nobis dolores! Vitae,
									in! In explicabo cupiditate tenetur!
								</Typography>
							</CardContent>
						</CardActionArea>
						<CardActions>
							<Button
								variant="contained"
								size="medium"
								color="primary"
								fullWidth={true}
								onClick={() => downloadApk(application.hash)}
							>
								Télécharger
							</Button>
						</CardActions>
					</Card>

					<ListeCommentaires application={application} />
				</>
			) : (
				<LoadingScreen props={10} />
			)}
		</>
	);
}
