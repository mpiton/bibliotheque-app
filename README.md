# Bibliothèque d'application mobile

Bibliothèque d'application mobile crée sous la stack MERN, les applications sont vérifiés avec VirusTotal avant d'être uploadé

## Installation

Utiliser le package manager [npm](https://nodejs.org/en/) pour installer l'application

<ins>A partir du dossier "backend":</ins>

```bash
npm run preinstall #pour installer les dépendances
```

## Usage

<ins>Dans le dossier "backend":</ins>

<ul>
<li>Renommer le fichier <b>.env.example</b> en <b>.env</b></li>
<li>Toujours à la racine du backend, créer un dossier nommé <b>uploads</b></li>
</ul>

<ins>A partir du dossier "backend":</ins>

```bash
npm start #lance le server
npm run dev #lance le server et le client
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
