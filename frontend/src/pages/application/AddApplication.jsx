import React, { useState } from "react";
import {
	Input,
	FormControl,
	InputLabel,
	Button,
	Fab,
	Paper,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import LinearProgress from "@material-ui/core/LinearProgress";
import CircularProgress from "@material-ui/core/CircularProgress";
import ReportProblemIcon from "@material-ui/icons/ReportProblem";
//Style
const useStyles = makeStyles((theme) => ({
	root: {
		"& > *": {
			margin: theme.spacing(1),
		},
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: 300,
	},
	paper: {
		display: "flex",
		alignItems: "center",
		justifyContent: "space-evenly",
	},
	error: {
		display: "flex",
		alignItems: "center",
		justifyContent: "space-evenly",
		color: "crimson",
	},
}));

const AddApplication = () => {
	//STATE Initial
	const initialApplicationState = {
		nom: "",
		fichier: undefined,
	};

	//STATES
	const [application, setApplication] = useState(initialApplicationState);
	const [selectedFile, setSelectedFile] = useState();
	const [message, setMessage] = useState();

	const classes = useStyles();

	//Recupére les infos au changement
	const handleInputChange = (event) => {
		const { name, value } = event.target;
		console.log(event.target.value);
		setApplication({ ...application, [name]: value });
		setMessage(error);
	};

	//Message en dessous du bouton
	const body = (
		<Paper className={classes.paper} variant="outlined">
			<p>Vérification antivirus en cours...</p> <CircularProgress />
		</Paper>
	);

	//Message en dessous du bouton
	const error = (
		<Paper className={classes.error} variant="outlined">
			<ReportProblemIcon />
			<p>Votre application semble infectée ou défectueuse</p>{" "}
			<ReportProblemIcon />
		</Paper>
	);

	const changeHandler = (event) => {
		setSelectedFile(event.target.files[0]);
		console.log(event.target.value);
	};

	// Fonction de création d'application
	const saveApplication = (e) => {
		e.preventDefault();
		const formData = new FormData();
		formData.append("file", selectedFile);
		formData.append("nom", application.nom);
		const config = {
			headers: {
				"content-type": "multipart/form-data",
			},
		};
		setMessage(body);
		axios
			.post("http://localhost:3333/api/v1/app", formData, config)
			.then((response) => {
				setMessage("Pas de virus, application mise en ligne.");
				window.location = "/";
			})
			.catch((error) => {
				setMessage(error.message);
			});
	}; //fin méthode save

	return (
		<div className="submit-form">
			<form onSubmit={saveApplication}>
				<FormControl margin="normal" fullWidth>
					<InputLabel htmlFor="applicationNom">Nom de l'application</InputLabel>
					<Input
						id="applicationNom"
						required={true}
						onChange={handleInputChange}
						name="nom"
						placeholder="Nom"
						inputProps={{ "aria-label": "nom de l'application" }}
					/>
				</FormControl>

				<FormControl align="center" margin="normal" fullWidth>
					<label htmlFor="applicationFichier">
						<input
							id="ApplicationFichier"
							name="file"
							type="file"
							required={true}
							onChange={changeHandler}
						/>
					</label>
				</FormControl>

				<div className={classes.root}>
					<Button
						fullWidth={true}
						variant="contained"
						color="primary"
						type="submit"
					>
						Ajouter
					</Button>
				</div>
			</form>
			{message ? message : null}
		</div>
	);
};

export default AddApplication;
