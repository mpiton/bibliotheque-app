import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import routeApplication from "../api/routes/routeApplication";
import Grid from "@material-ui/core/Grid";
import LoadingScreen from "./LoadingScreen";

//Style du component
const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		maxWidth: 345,
		marginTop: 20,
		cursor: "default",
	},
});

export default function ListeApplications() {
	const classes = useStyles();

	//States
	const [applications, setApplications] = useState([]);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		retrieveApplications();
		setTimeout(() => setLoading(false), 6000); //fake loading le temps que les data se chargent
	}, []);

	//Récupération de toutes les applications
	const retrieveApplications = () => {
		routeApplication
			.getAll()
			.then((response) => {
				setApplications(response.data.applications);
			})
			.catch((e) => {
				console.log(e);
			});
	};

	return (
		<React.Fragment>
			{loading === false ? (
				<Grid container spacing={3}>
					{applications && applications.length > 0 ? (
						applications.map((application, i) => (
							<Grid key={i} item xs={3}>
								<Card style={{ cursor: "default" }} className={classes.root}>
									<CardActionArea>
										<CardMedia
											style={{ cursor: "default" }}
											component="img"
											alt="Image par defaut de l'application"
											height="140"
											image="/images/default.jpg"
											title={application.nom}
										/>

										<CardContent style={{ cursor: "default" }}>
											<Typography gutterBottom variant="h5" component="h2">
												{application.nom}
											</Typography>

											<Typography
												variant="body2"
												color="textSecondary"
												component="p"
											>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit.
												Vestibulum quis dictum ante, vitae sodales metus. Proin
												pharetra tellus a pulvinar ultricies.
											</Typography>
										</CardContent>
									</CardActionArea>
									<CardActions>
										<Button
											style={{ cursor: "pointer" }}
											href={`application/${application.hash}`}
											size="small"
											color="primary"
										>
											En savoir plus
										</Button>
									</CardActions>
								</Card>
							</Grid>
						))
					) : (
						<Typography
							style={{ margin: "0 auto" }}
							variant="h3"
							component="h2"
						>
							Pas encore d'application
						</Typography>
					)}
				</Grid>
			) : (
				<LoadingScreen props={10} />
			)}
		</React.Fragment>
	);
}
