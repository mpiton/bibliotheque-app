import { Document } from "mongoose";
import { ICommentaire } from "./commentaire";

export interface IApplication extends Document {
	nom: string;
	commentaires: ICommentaire[];
	hash: string;
	fichier: {
		fileName: string;
		filePath: string;
		fileType: string;
		fileSize: string;
	};
}
