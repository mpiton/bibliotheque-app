import { Router } from "express";
import {
	addCommentaire,
	getCommentaireByApplicationHash,
	deleteCommentaire,
} from "../controllers/CommentaireController";

const router: Router = Router();

//** Commentaire
//Création d'une application
router.post("/commentaire", addCommentaire);
//Delete commentaire by id
router.delete("/commentaire/:id", deleteCommentaire);

export default router;
