import api from "../api.js";
import download from "../download.js";

//GET ALL applications
const getAll = () => {
	return api.get("/apps");
};

//Get application by hash
const get = (hash) => {
	return api.get(`/app/${hash}`);
};

//Création de l'application
const create = (data, config) => {
	return api.post("/app", data, config);
};

//Téléchargement de l'application
const downloadApp = (hash) => {
	return download.get(`download/${hash}`);
};

export default {
	getAll,
	get,
	create,
	downloadApp,
};
