import { Document } from "mongoose";

export interface ICommentaire extends Document {
	titre: string;
	commentaire: string;
	application: string;
}
