import { Response, Request } from "express";
import { IApplication } from "../types/application";
import Application from "../models/Application";
import crypto from "crypto";
import scanFichier from "../helpers/virusTotal";

// Ajout d'une application
const addApplication = async (req: Request, res: Response): Promise<void> => {
	const file = {
		fileName: req.file.originalname,
		filePath: req.file.path,
		fileType: req.file.mimetype,
		fileSize: fileSizeFormatter(req.file.size, 2), //formate le poids à 2 nombres après la virgule -> 0.00
	};

	//Si le format du fichier n'est pas correct il sera undefined
	if (file === undefined) {
		res.status(400).json({ message: "L'upload du fichier a échoué" });
	} else {
		//Scan VirusTotal du fichier
		const scan = await scanFichier(file).then((resultatDuScan) => {
			//Si le résultat est true (fichier infecté) ou undefined (erreur)
			if (resultatDuScan !== false) {
				res.status(401).json({ message: "Le fichier semble être infecté" });
			} else {
				//Création du hash
				var hash = crypto.randomBytes(20).toString("hex");
				//Inscription de l'application en BDD
				const application = new Application({
					nom: req.body.nom,
					commentaire: req.body.commentaire,
					hash: hash,
					fichier: file,
				});
				application.save((error, app) => {
					if (error) {
						return res.status(500).json({
							message: error.message,
						});
					}
					res.status(201).json(app);
				});
			}
		});
	}
};
//GET ALL Produits
const getApplications = async (req: Request, res: Response): Promise<void> => {
	try {
		const applications: IApplication[] = await Application.find();
		if (!applications) {
			res.status(404).json({ message: "Aucune application trouvé" });
		}
		res.status(200).json({ applications });
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET Application BY hash
const getApplicationByHash = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		const application: IApplication = (await Application.findOne({
			hash: req.params.hash,
		})) as IApplication;
		if (!application) {
			res.status(404).json({ error: "Application non trouvé" });
		} else {
			res.status(200).json({ application });
		}
	} catch (error) {
		res.status(401).json({ message: "Hash invalide" });
	}
};

//Download application
const downloadApplication = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		const application: IApplication = (await Application.findOne({
			hash: req.params.hashApp,
		})) as IApplication;
		console.log(application);

		if (!application) {
			res.status(404).json({ error: "Application non trouvé" });
		} else {
			const path = application.fichier.filePath;
			console.log(path);

			res.download(path, application.fichier.fileName);
		}
	} catch (error) {
		res.status(401).json({ message: "Hash invalide" });
	}
};

//Formate le poids du fichier
const fileSizeFormatter = (bytes: number, decimal: number) => {
	if (bytes === 0) {
		return "0 Bytes";
	} else {
		const dm = decimal || 2;
		const size = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB"];
		const index = Math.floor(Math.log(bytes) / Math.log(1000));
		return (
			parseFloat((bytes / Math.pow(1024, index)).toFixed(dm)) +
			"-" +
			size[index]
		);
	}
};

export {
	addApplication,
	getApplications,
	getApplicationByHash,
	downloadApplication,
};
