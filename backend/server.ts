import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";
import morgan from "morgan";
import cors from "cors";
import { connect } from "./config/db";
import path from "path";
import appRoutes from "./routes/application";
import commentaireRoutes from "./routes/commentaire";

dotenv.config();

const PORT = process.env.PORT || 3333;
const app: Express = express();

// Connexion à la BDD
connect();

//Body Parsing Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));

//Set le root path
app.set("rootDirectory", __dirname);
//Lien vers les routes
app.use("uploads", express.static(path.join(__dirname, "uploads")));
app.use("/api/v1", appRoutes);
app.use("/api/v1", commentaireRoutes);

app.listen(PORT, () => console.log(`Running on ${PORT} ⚡`));
