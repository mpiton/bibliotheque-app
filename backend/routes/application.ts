import { Router } from "express";
import {
	addApplication,
	downloadApplication,
	getApplicationByHash,
	getApplications,
} from "../controllers/ApplicationController";
import { upload } from "../helpers/upload";

const router: Router = Router();

//** Application
//GET ALL applications
router.get("/apps", getApplications);
//Création d'une application
router.post("/app", upload.single("file"), addApplication);
//GET application par son hash
router.get("/app/:hash", getApplicationByHash);

//Téléchargement de l'application
router.get("/download/:hashApp", downloadApplication);

//Params Application
router.param("hash", getApplicationByHash);
router.param("hashApp", downloadApplication);

export default router;
