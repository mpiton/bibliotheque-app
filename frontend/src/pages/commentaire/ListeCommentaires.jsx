import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { Avatar, Link, Paper, Typography } from "@material-ui/core";
import AddCommentaire from "./AddCommentaire";
import { makeStyles } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";
import routeCommentaire from "../../api/routes/routeCommentaire";

const imgLink =
	"https://images.pexels.com/photos/1681010/pexels-photo-1681010.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260";

const useStyles = makeStyles({
	delete: {
		color: red.A700,
		alignSelf: "center",
		cursor: "pointer",
	},
});

function ListeCommentaires(application) {
	const [commentaires, setCommentaires] = useState([]);
	const classes = useStyles();
	useEffect(() => {
		setCommentaires(application.application.commentaires);
	}, []);

	//Suppression d'un commentaire
	const deleteCommentaire = (com) => {
		routeCommentaire
			.remove(com._id)
			.then((response) => {
				window.location = `/application/${application.application.hash}`;
			})
			.catch((e) => {
				console.log(e);
			});
	};

	return (
		<div style={{ padding: 14, maxWidth: "85%" }} className="App">
			<Typography style={{ marginTop: 10 }} variant="h4" component="h2">
				Commentaires:
			</Typography>

			<AddCommentaire application={application} />

			{commentaires !== [] ? (
				commentaires.map((commentaire, i) => (
					<Paper key={i} style={{ padding: "40px 20px", marginTop: 30 }}>
						<Grid container wrap="nowrap" spacing={2}>
							<Grid item>
								<Avatar alt="logo de l'utilisateur" src={imgLink} />
							</Grid>
							<Grid item xs zeroMinWidth>
								<h4 style={{ margin: 0, textAlign: "left" }}>
									{commentaire.titre}
								</h4>
								<p style={{ textAlign: "left" }}>{commentaire.commentaire} </p>
								<p style={{ textAlign: "left", color: "gray" }}>
									posté le{" "}
									{new Date(commentaire.createdAt).toLocaleDateString("fr-FR")}
									{" à "}
									{new Date(commentaire.createdAt)
										.toJSON()
										.substring(10, 16)
										.replace("T", " ")}
								</p>
							</Grid>
							<Link
								onClick={() => deleteCommentaire(commentaire)}
								className={classes.delete}
							>
								Supprimer
							</Link>
						</Grid>
					</Paper>
				))
			) : (
				<Paper>
					<Typography
						style={{ marginTop: 10, textAlign: "center" }}
						variant="h6"
						component="h3"
					>
						Il n'y a pas encore de commentaires
					</Typography>
				</Paper>
			)}
		</div>
	);
}

export default ListeCommentaires;
