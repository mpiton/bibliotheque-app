import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Button from "@material-ui/core/Button";
import AddApplication from "../pages/application/AddApplication";
import { Modal } from "@material-ui/core";

//Style du component
const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	addButton: {
		position: "absolute",
		right: theme.spacing(2),
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
		"@media only screen and (min-device-width : 280px) and (max-device-width : 320px)": {
			paddingTop: theme.spacing(3),
			paddingBottom: theme.spacing(3),
			paddingRight: "0",
			paddingLeft: "0",
		},
	},
	paper: {
		position: "absolute",
		width: 400,
		backgroundColor: theme.palette.background.paper,
		border: "2px solid #000",
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
	},
}));

//Style de la modal
function getModalStyle() {
	const top = 30;
	const left = 50;
	return {
		top: `${top}%`,
		left: `${left}%`,
		transform: `translate(-${top}%, -${left}%)`,
	};
}

export default function Header() {
	const classes = useStyles();

	//STATES
	const [modalStyle] = React.useState(getModalStyle);
	const [open, setOpen] = React.useState(false);

	//Ouverture modal
	const handleOpen = () => {
		setOpen(true);
	};

	//Fermeture modal
	const handleClose = () => {
		setOpen(false);
	};

	//Contenu de la modal
	const body = (
		<div style={modalStyle} className={classes.paper}>
			<AddApplication />
		</div>
	);

	return (
		<>
			<div className={classes.root}>
				<AppBar position="static">
					<Toolbar variant="dense">
						<IconButton
							edge="start"
							className={classes.menuButton}
							color="inherit"
							aria-label="menu"
							href="/"
						>
							<MenuIcon />
						</IconButton>
						<Typography variant="h6" color="inherit">
							Bibliothèque d'applications
						</Typography>
						<Button
							className={classes.addButton}
							variant="contained"
							color="secondary"
							onClick={handleOpen}
						>
							Ajouter une application
						</Button>
					</Toolbar>
				</AppBar>
			</div>
			<main className={classes.content}>
				<Modal
					open={open}
					onClose={handleClose}
					aria-labelledby="simple-modal-title"
					aria-describedby="simple-modal-description"
				>
					{body}
				</Modal>
			</main>
		</>
	);
}
