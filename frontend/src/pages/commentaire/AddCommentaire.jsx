import { Button, Paper, TextField } from "@material-ui/core";
import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import routeCommentaire from "../../api/routes/routeCommentaire";

const useStyles = makeStyles({
	multiline: {
		fontWeight: "bold",
		fontSize: "20px",
		color: "purple",
		width: "50vw",
	},
	commentaire: {
		marginBottom: 20,
		minWidth: "92%",
	},
});

export default function AddCommentaire(application) {
	const initialCommentaire = {
		titre: "",
		commentaire: "",
		applicationId: "",
	};
	const classes = useStyles();
	const [commentaire, setCommentaire] = useState(initialCommentaire);

	//Recupére les infos au changement
	const handleInputChange = (event) => {
		const { name, value } = event.target;
		setCommentaire({ ...commentaire, [name]: value });
	};

	const saveCommentaire = () => {
		//Initialisation des champs
		var data = {
			titre: commentaire.titre,
			commentaire: commentaire.commentaire,
			applicationId: application.application.application._id,
		};

		//Ajout de la donnée reçu dans les bon champs
		routeCommentaire
			.create(data)
			.then((response) => {
				window.location = `/application/${application.application.application.hash}`;
			})
			.catch((e) => {
				console.log(e);
			});
	}; //fin méthode save

	return (
		<Paper style={{ textAlign: "center", marginTop: 20 }}>
			<TextField
				required
				className={classes.commentaire}
				label="Titre"
				variant="outlined"
				value={commentaire.titre}
				onChange={handleInputChange}
				inputProps={{ "aria-label": "titre du commentaire" }}
				name="titre"
			/>
			<TextField
				required
				className={classes.multiline}
				id="outlined-multiline-static"
				label="Commentaire"
				multiline
				rows={10}
				name="commentaire"
				variant="outlined"
				value={commentaire.commentaire}
				onChange={handleInputChange}
				inputProps={{ "aria-label": "commentaire de l'utilisateur" }}
			/>
			<Button
				style={{ marginTop: 20 }}
				className={classes.commentaire}
				variant="contained"
				color="primary"
				onClick={saveCommentaire}
			>
				Envoyer
			</Button>
		</Paper>
	);
}
