import React, { useState } from "react";
import "./App.css";
import Header from "./layout/Header";
import { Container } from "@material-ui/core";
import ListeApplications from "./pages/ListeApplications";
import { Switch, Route } from "react-router-dom";
import OneApplication from "./pages/OneApplication";

function App() {
	return (
		<div className="app">
			<Switch>
				<Route exact path="/">
					<Header />
					<Container>
						<ListeApplications />
					</Container>
				</Route>

				{/* Affichage d'une application */}
				<Route exact path="/application/:hash">
					<Header />
					<Container>
						<OneApplication />
					</Container>
				</Route>

				{/*Reste des routes*/}
				<Route path="*" render={() => "404 not found!"} />
			</Switch>
		</div>
	);
}

export default App;
