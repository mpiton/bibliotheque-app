import { ICommentaire } from "../types/commentaire";
import { model, Schema } from "mongoose";

export const commentaireSchema: Schema<ICommentaire> = new Schema(
	{
		titre: {
			type: String,
			required: [true, "Le champs titre est obligatoire."],
		},
		commentaire: {
			type: String,
			required: [true, "Le champs commentaire est obligatoire."],
		},
		appplicationId: {
			type: String,
			require: true,
		},
	},
	{ timestamps: true }
);

const Commentaire = model<ICommentaire>("commentaires", commentaireSchema);
export default Commentaire;
