import { IApplication } from "../types/application";
import { model, Schema } from "mongoose";
import { commentaireSchema } from "./Commentaire";

//Model D'une Application
export const applicationSchema: Schema<IApplication> = new Schema(
	{
		nom: {
			type: String,
			required: [true, "Le champs nom est obligatoire."],
		},
		//ref au model Commentaire
		commentaires: [commentaireSchema],
		hash: {
			type: String,
			unique: [true, "Ce hash existe déjà."],
		},
		fichier: {
			type: Object,
			required: true,
		},
	},
	{ timestamps: true }
);

const Application = model<IApplication>("applications", applicationSchema);
export default Application;
