import { Response, Request } from "express";
import Joi from "joi";
import { ICommentaire } from "../types/commentaire";
import Commentaire from "../models/Commentaire";
import dotenv from "dotenv";
import Application from "../models/Application";
import { IApplication } from "./../types/application";

//Pattern de validation
const schema = Joi.object().keys({
	titre: Joi.string().min(2).max(30).required(),
	commentaire: Joi.string().min(2).max(1000).required(),
	applicationId: Joi.string().lowercase().min(2).max(45).required(),
});

// ADD COMMENTAIRE
const addCommentaire = async (req: Request, res: Response): Promise<void> => {
	try {
		const { titre, commentaire, applicationId } = await req.body;

		//Try validation
		const result = schema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		//Si la validation ne passe pas
		if (!valid) {
			res.status(401).json({
				message: "Requête invalide: Erreur de validation",
				data: req.body,
				error: error,
			});
		} else {
			//Création du commentaire
			const myCommentaire = new Commentaire({
				titre,
				commentaire,
				applicationId,
			});
			//Mise à jour de l'application pour lier le commentaire
			const app = await Application.findOne({ _id: applicationId });
			if (app) {
				app.commentaires.push(myCommentaire);
				app.save();
			}
			//Sauvegarde du commentaire
			myCommentaire.save((error, commentaire) => {
				if (error) {
					return res.status(500).json({
						error,
					});
				}
				res.status(201).json(commentaire);
			});
		}
	} catch {
		res.status(500).json({ message: "Il y a eu un problème d'envoi." });
	}
};

//GET ALL Commentaires d'une application
const getCommentaireByApplicationHash = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		const commentaires: IApplication[] = await Application.find({
			hash: req.params.hash,
		}).populate("Commentaire");
		if (!commentaires) {
			res.status(404).json({ message: "Aucun commentaire trouvé" });
		}
		res.status(200).json({ commentaires });
	} catch (error) {
		res.status(500).send(error);
	}
};

//DELETE Commentaire
const deleteCommentaire = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		const deletedCommentaire: ICommentaire | null = await Commentaire.findById(
			req.params.id
		);
		if (deletedCommentaire) {
			await Application.updateMany(
				{},
				{
					$pull: {
						commentaires: {
							titre: deletedCommentaire.titre,
							commentaire: deletedCommentaire.commentaire,
						},
					},
				},
				{ multi: true }
			);
			deletedCommentaire.remove();
			res.status(200).json({
				message: "Commentaire supprimée",
				commentaire: deletedCommentaire,
			});
		} else {
			res.status(404).json({ message: "Commentaire non trouvé" });
		}
	} catch (error) {
		res.status(401).json({ message: "Vous ne pouvez pas faire ceci" });
	}
};

export { addCommentaire, getCommentaireByApplicationHash, deleteCommentaire };
