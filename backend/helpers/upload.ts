import multer from "multer";

//Configuration de l'enregistrement du fichier APK
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, "uploads");
	},
	filename: (req, file, cb) => {
		cb(
			null,
			new Date().toISOString().replace(/:/g, "-") + "-" + file.originalname
		);
	},
});

//Vérifie que le fichier est bien un APK
const fileFilter = (
	_req: any,
	file: { mimetype: string },
	cb: (arg0: null, arg1: boolean) => void
) => {
	//Si APK -> Upload
	if (file.mimetype === "application/vnd.android.package-archive") {
		cb(null, true);
	} else {
		//sinon pas d'upload
		cb(null, false);
	}
};

export const upload = multer({ storage: storage, fileFilter: fileFilter });
